---
title: What is referential transparency in essence?
date: "2019-12-08T20:46:37.121Z"
layout: post
category: "编程"
description: Referential transparency is a core concept of functional programming, but its real meaning is obscure, especially to newcomers. This article delves into this concept, trying to the reveal its very essence. 
---

### Introduction
Referential transparency is a fundamental concept of functional programming, but its real meaning is obscure, especially to newcomers. This article delves into this concept, trying to the reveal its very essence.

In the following, we sometimes also use RT as a shorthand for referential transparency.

### Referential tranparent expression
First of all, we need to clarify the context of discussion, as much of obscurity arise from the lack of or unclear context. When we say something is referential transparent, it must be an expression. Someone take it for granted, because they think in functional programming way, where expression is a first class citizen. However, this is not true for most programmers that get used to procedural programming languages. For a long time, I do not understand RT because I come up with a Java statement, and ask myself what is a RT version of it.

**Definition:**
**An _expression_ in a program, is _referential transparent_ if it may be replaced by its value (or anything having the same value) without changing the program behaviors.**

#### Examples
1. Below is a simple scala program 
```scala
val a = {
    println("hi"); 1
}
a + a
```
When this program runs, it will print "hi" once, and the final result is `a + a = 2`.

In addition, if we replace _a_ with its value, the program becomes
```scala
({ println("hi"); 1}) + ({println("hi"); 1})
```
This program prints "hi" twice run it runs. Its behavior is different to the original one. Thus, _a_ is *not* referential transparent.

2. The expression _a_ in the following program is referential transparent.
```scala
val a = 1
a + a
```
If we replace _a_ by its value `1`, the behaviors of the program are the same.
<!--
3. The RT of an expression is affected by the RT of its subexpressions.
```scala
val s = println("hi")
val t = {s; s}
```
It is not difficult to verify that, both _s_ and _t_ are not referential transparent in this program.
-->

#### Theorems
1. A referential transparent expression cannot have any side effects.
> Short proof: give example 1 as a counter example.
<!--
2. All subexpressions of a referential transparent exression must be referential transparent.
> Short proof: give example 3 as a counter example.

Theorems 1 and 2 shows some necessary conditions of a RT expression, which is quite strict, as we can see.
-->

### Referential transparency FTW
Suppose we are functional programming advocates, and we believe in writing pure code for the win, how do we achieve that goal?

It turns out that, the heart of the problems is side effect handling. In any realworld programs, we have to deal with side effect, like printing as previous example shows. Fortunately, there are already solutions to this problem. That is effect libraries in Scala, and the built-in IO monad in Haskell.

Let us take Cats effect library as an example, and rewrite example 1 into a referential transparent version.

```scala
import cats.effect._
val a = IO {
    println("hi"); 1
}
val b = for {
    x <- a
    y <- a
} yield x + y
b.unsafeRunSync()
```
The main difference is the introduction of `IO`. Just like any other effect library, `IO` wraps any expressions that may contain side effect into a pure value. Its signature is as below.
```scala
object IO {
    def apply[A](body: => A): IO[A]
}
```
Since _a_ is of type `IO[Int]` now, we cannot use the `+` operator any more. Instead, we build a new effect value _b_ from _a_. For demonstration purpose, we invoke the `unsafeRunSync` method to trigger running the effect.

Now it is easy to verify that _a_ is referential transparent.

### How does effect libraries work?
An _effect type_ (and value) is introduced, that can wrap an expression with side effect into a referential transparent expression. Aforementioned `IO` type is such an example.
But how does such wrapping work? It sounds like a magic, isn't it?

The key is, an _effect value_ is actually **lazily evaluated computation**. Thus, a value of type `IO[Int]` represents a computation that, when run, will do some work (having side effects), and finally produce an `Int` as the result. In this regard, `IO[A]` is equivalent to `() => A`.
```scala
type IO[A] = () => A
```

**Proposition**

**Any expression of the type `() => A` is _referential transparent_.**

#### Location independence
Recall that in example 1, the side effect arises from `println("hi")`. Furthermore, the signature of `println` is the following.
```scala
def println(s: String) => Unit
```
In other words, the expression `println("hi")` is eagerly evaluated. This difference makes me suspect that RT is related to eagerness of expression.

**If one expression has side effects and is eagerly evaluated, then its behavior depends on the location of the expression in the program. Otherwise, when an expression is lazily evaluated, its effect occurs deterministically regardless of its location, thus you can move it anywhere, aka referential transparent.**

To help you understand the statements, let me give an example.
This program prints a before b.
```scala
println("a")
println("b")
```
If we want to reuse `println("b")`, and give it a name (reference) like the following
```scala
val stm = println("b")
println("a")
stm
```
This program prints "b" before "a". Because the expression `println("b")` is eagerly evaluated, moving it around or replacing it by a reference may alter its behavior, depending on its final location.

On the other hand, what happen if `println` is lazily evaluated?
```scala
def printAction(s: String) = () => println(s)
val a = printAction("a")
val b = printAction("b")
a()
b()
```
In this case, expression _a_ and _b_ can be in any order, and the program behaviors keep the same. Surprise, we make _a_ and _b_ referential transparent without any effect library.

Therefore, _referential transparency_ is equivalent to _location independence_.

#### Explicit (expression) dependency
In procedural programming languages, a _program_ is a _sequence_ of statements (instructions). Consequently, the order of statements matters a lot. On the other hand, in functional programming languages, a _program_ is a _lambda expression_. Thus, expression order is irrelevant to its semantics.

In real life, things come in order most of the time. Say we want to print a then b, we do this in procedural language like this.
```scala
println("a")
println("b")
```
As we can see, we do not specify the order of execution, rather it is **implicitly** defined (as code order).

To achieve the same thing in functional programming, we write something like the following.
```scala
val a = IO{println("a")}
val b = IO{println("b")}
for {
    _ <- a
    _ <- b
} yield ()
```
The `for` expression actually desugars to `a.flatMap(_ => b)`. Note the `flatMap` call, it **explicitly** encodes the order (dependency) of _a_ and _b_.

In conclusion, **_referential transparency_ = _location independence_ = _explicit (expression) dependency_**

### Benefits of RT
- Local reasoning
- Easy code refactoring
- Explicit value dependency

### Drawbacks of RT
- Deep learning curve
- Viral IO boilerplate
- Performance overhead

### Conclusion
I think _explicit dependency_ captures the essence of referential transparency. Because the expression dependency is explicit, it is easier to reason about the logic, and safer to refactor code. Also because of this explicitness, we have to write more code to express it.

After learning the very essence of RT, I will embrace this elegant concept, but I won't force myself to write referential transparent program in real world application.
